<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Masterdigm_Controller_Dashboard extends Masterdigm_Init{
	public $is_ready = false;
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function controller($action = ''){
		$md_install_notice_obj = new Masterdigm_Controller_InstallNotice;
		if( trim($action) == '' ){
			$action = $this->get_action();
		}
		switch($action){
			default:
				if( !md_has_api_credentials() && !md_has_passed_prerequisite() ){
					$md_install_notice_obj->controller();
				}else{
					if(
						md_has_api_credentials() &&
						md_has_passed_prerequisite() &&
						md_finish_subscribe()
					){
						$this->index();
					}else{
						Masterdigm_Controller_Subscribe::get_instance()->controller();
					}
				}
			break;
		}
	}

	public function index(){
		$dashboard_url = admin_url( 'admin.php?page=' . md_plugin_name() . '&tab=dashboard' );
		$tab = $this->nav_tab();
		$template = 'dashboard';
		require_once md_admin_partials() . 'dashboard/dashboard-index.php';
	}

	public function __construct(){}
}

