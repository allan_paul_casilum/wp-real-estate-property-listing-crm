<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Masterdigm_Controller_InstallNotice extends Masterdigm_Init{
	protected static $instance = null;
	public $md_setup_finish = false;
	public $md_setup_prerequisite = false;
	public $md_setup_api = false;
	public $md_setup_settings = false;
	public $md_current_setup = false;
	public $md_install_notice_obj;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function display_setup_prerequisite(){
		$cache_folder = false;
		$permalinks = false;
		$pages = false;
		$step_one = $this->md_install_notice_obj->setup_prerequisite();
		$plugin = new WP_Real_Estate_Property_Listing();
		if( isset($step_one['folder']) && count($step_one['folder']) > 0 ){
			$cache_folder = $step_one['folder'];
		}
		if( isset($step_one['permalinks']) && count($step_one['permalinks']) > 0 ){
			$permalinks = $step_one['permalinks'];
		}
		if( isset($step_one['wp_page']) && count($step_one['wp_page']) > 0 ){
			$pages = $step_one['wp_page'];
		}
		require_once md_admin_partials() . 'setup-prerequisite.php';
	}

	public function controller($action = ''){
		$this->md_install_notice_obj = new Masterdigm_InstallNotice;
		if( trim($action) == '' ){
			$action = $this->get_action();
		}
		switch($action){
			default:
				if(
					!is_bool($this->md_install_notice_obj->setup_prerequisite())
					&& is_array($this->md_install_notice_obj->setup_prerequisite())
				){
					$this->display_setup_prerequisite();
				}else{
					$this->md_install_notice_obj->has_passed_prerequisite('u', 1);
					Masterdigm_Controller_Subscribe::get_instance()->controller();
				}
			break;
		}
	}

	public function __construct(){}
}
