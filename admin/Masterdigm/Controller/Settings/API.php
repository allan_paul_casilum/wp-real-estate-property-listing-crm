<?php
/**
 * API Keys
 * */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Masterdigm_Controller_Settings_API extends Masterdigm_Init{
	protected static $instance = null;
	public $mode_settings_api;
	public $tab_content = 'settings-crm';
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {
		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	public function controller($action = ''){
		if( trim($action) == '' ){
			$action = $this->get_action();
		}
		switch($action){
			case 'update_api':
				$this->update_api();
				$this->index();
			break;
			default:
				$this->index();
			break;
		}
	}

	public function update_api(){
		$error = array();
		$has_error = false;
		$post = $_POST;
		if(
			( trim($post['settings_api_key']) == '' || trim($post['settings_api_token']) == '')
		){
			$has_error = true;
			$error[] = __('Please provide property CRM api token / key', md_localize_domain());
		}

		if(
			$post['settings_api_default_feed'] == '0'
		){
			$has_error = true;
			$error[] = __('Please choose data to feed', md_localize_domain());
		}

		if(
			$post['settings_api_broker_id'] == ''
		){
			$has_error = true;
			$error[] = __('Please provide Broker ID', md_localize_domain());
		}

		if( $has_error ){
			$this->set_input_error($error);
		}else{
			$key = sanitize_text_field($post['settings_api_key']);
			$token = sanitize_text_field($post['settings_api_token']);
			$broker_id = sanitize_text_field($post['settings_api_broker_id']);
			$default_feed = sanitize_text_field($post['settings_api_default_feed']);

			$this->model_settings_api->key('u', $key);
			$this->model_settings_api->token('u', $token);
			$this->model_settings_api->broker_id('u', $broker_id);
			$this->model_settings_api->default_feed('u', $default_feed);
			return true;
		}
	}

	public function index(){
		$tab_content = $this->tab_content;
		$url = admin_url( 'admin.php?page=' . md_admin_current_page() . '&tab='.$tab_content.'' );
		$tab = $this->nav_tab();

		$template = 'settings';
		$default_tab_content = $template;
		if( !md_get_current_tab() ) {
			$default_tab_content = $tab_content;
		}
		$content_data['url'] = $url;
		$content_data['tab'] = $default_tab_content;
		$content_data['settings_api_key'] = $this->model_settings_api->key('r');
		$content_data['settings_api_token'] = $this->model_settings_api->token('r');
		$content_data['settings_api_broker_id'] = $this->model_settings_api->broker_id('r');
		$content_data['settings_api_default_feed'] = $this->model_settings_api->default_feed('r');
		$content_data['error'] = $this->get_input_error();
		require_once Masterdigm_Controller_Settings_Init::get_instance()->view();
	}
	
	public function form(){
		$url_slug = get_admin_url() . $this->model_settings_api->url_slug();
		$api_feed = $this->model_settings_api->default_feed('r');
		$key = $this->model_settings_api->key('r');
		$token = $this->model_settings_api->token('r');
		$broker_id = $this->model_settings_api->broker_id('r');
		
		require_once md_dir_admin('admin_dir_partials') . '/settings/settings-crm.php';
	}
	
	public function __construct(){
		$this->model_settings_api = new Masterdigm_Settings_API;
	}
}

