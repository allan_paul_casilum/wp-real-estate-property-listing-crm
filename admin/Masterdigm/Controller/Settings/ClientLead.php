<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Masterdigm_Controller_Settings_ClientLead extends Masterdigm_Init{
	protected static $instance = null;
	public $model;
	public $tab_content = 'settings-client-lead';
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {
		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	public function index(){
		$tab_content = $this->tab_content;
		$url = admin_url( 'admin.php?page=' . md_admin_current_page() . '&tab='.$tab_content.'' );
		$tab = $this->nav_tab();

		$template = 'settings';
		$default_tab_content = $template;
		if( !md_get_current_tab() ) {
			$default_tab_content = $tab_content;
		}

		$content_data['url'] = $url;
		$content_data['tab'] = $default_tab_content;
		$content_data['md_lead_status'] = md_get_lead_status();
		$content_data['md_lead_type'] = md_get_lead_type();
		$content_data['model_lead'] = $this->model;
		require_once Masterdigm_Controller_Settings_Init::get_instance()->view();
	}

	public function controller($action = ''){
		if( trim($action) == '' ){
			$action = $this->get_action();
		}
		switch($action){
			case 'update':
				$status = sanitize_text_field($_POST['md_lead_status']);
				$type = sanitize_text_field($_POST['md_lead_type']);
				$this->model->lead_type('u', $type);
				$this->model->lead_status('u', $status);
				$this->index();
			break;
			default:
				$this->index();
			break;
		}
	}

	public function __construct(){
		$this->model = new Masterdigm_Settings_ClientLead;
	}
}

