<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Masterdigm_Controller_Settings_Init extends Masterdigm_Init{
	protected static $instance = null;
	public $obj_settings_property;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {
		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	public function view(){
		return md_admin_partials() . 'settings/settings-index.php';
	}

	public function controller($action = ''){
		if( trim($action) == '' ){
			$action = $this->get_action();
		}
		switch($action){
			default:
				$this->index();
			break;
		}
	}

	public function index(){
		$dashboard_url = admin_url( 'admin.php?page=' . md_admin_current_page() . '&tab=settings' );
		$tab = $this->nav_tab();

		$template = 'settings';
		$default_tab_content = $template;
		if( !md_get_current_tab() ) {
			$default_tab_content = 'settings-crm';
		}
		$content_data = array();
		require_once $this->view();
	}


	public function __construct(){}
}

