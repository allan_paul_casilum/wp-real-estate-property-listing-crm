<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Masterdigm_Controller_Settings_Mail extends Masterdigm_Init{
	protected static $instance = null;
	public $model;
	public $tab_content = 'settings-mail';
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {
		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	public function index(){
		$tab_content = $this->tab_content;
		$url = admin_url( 'admin.php?page=' . md_admin_current_page() . '&tab='.$tab_content.'' );
		$tab = $this->nav_tab();
		$template = 'settings';
		$default_tab_content = $template;
		if( !md_get_current_tab() ) {
			$default_tab_content = $tab_content;
		}

		$content_data['url'] = $url;
		$content_data['tab'] = $default_tab_content;
		$content_data['md_settings_mail_server'] = $this->model->get_mail_server();
		$content_data['content'] = stripslashes($this->model->md_settings_mail_content('r'));
		$content_data['subject'] = $this->model->md_settings_mail_subject('r');
		$content_data['editor_id'] = 'md_settings_mail_content';
		$content_data['settings'] = array( 'media_buttons' => false, 'editor_height' => 250, 'teeny' => true );

		require_once Masterdigm_Controller_Settings_Init::get_instance()->view();
	}

	public function update($post){
		$error = array();
		$has_error = false;

		$mail_server = $this->model->get_mail_server();
		if( trim($post['md_settings_mail_server']) != '' && is_email($post['md_settings_mail_server']) ){
			$mail_server = $post['md_settings_mail_server'];
		}
		$this->model->md_settings_mail_server('u', $mail_server);

		$subject = $this->model->md_settings_mail_subject('r');
		if( trim($post['md_settings_mail_subject']) != '' ){
			$subject = $post['md_settings_mail_subject'];
		}
		$this->model->md_settings_mail_subject('u', $subject);

		$content = $this->model->md_settings_mail_content('r');
		if( trim($post['md_settings_mail_content']) != '' ){
			$content = $post['md_settings_mail_content'];
		}
		$this->model->md_settings_mail_content('u', $content);
	}

	public function controller($action = ''){
		if( trim($action) == '' ){
			$action = $this->get_action();
		}
		switch($action){
			case 'update':
				$this->update($_POST);
				$this->index();
			break;
			case 'auto_add_data':
				$this->model->put_content();
				$this->model->put_subject();
			break;
			default:
				$this->index();
			break;
		}
	}

	public function __construct(){
		$this->model = new Masterdigm_Settings_Mail;
	}
}

