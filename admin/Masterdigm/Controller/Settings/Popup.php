<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Masterdigm_Controller_Settings_Popup extends Masterdigm_Init{
	protected static $instance = null;
	public $model;
	public $tab_content = 'settings-popup';
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {
		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	public function index(){
		$tab_content = $this->tab_content;
		$url = admin_url( 'admin.php?page=' . md_admin_current_page() . '&tab='.$tab_content.'' );
		$tab = $this->nav_tab();

		$template = 'settings';
		$default_tab_content = $template;
		if( !md_get_current_tab() ) {
			$default_tab_content = $tab_content;
		}

		$content_data['url'] = $url;
		$content_data['tab'] = $default_tab_content;
		$content_data['show_popup_choose'] = $this->model->show_popup_choose();
		$content_data['show_popup_close_button'] = $this->model->show_popup_close_button();
		$content_data['show_popup_after'] = $this->model->show_popup_after();
		$content_data['md_popup_show'] = $this->model->md_popup_show('r');
		$content_data['md_popup_close'] = $this->model->md_popup_close('r');
		$content_data['md_popup_clicks'] = $this->model->md_popup_clicks('r');

		require_once Masterdigm_Controller_Settings_Init::get_instance()->view();
	}

	public function update($post){
		$this->model->md_popup_show('u', $post['md_popup_show']);
		$this->model->md_popup_close('u', $post['md_popup_close']);
		$this->model->md_popup_clicks('u', $post['md_popup_clicks']);
	}

	public function controller($action = ''){
		if( trim($action) == '' ){
			$action = $this->get_action();
		}
		switch($action){
			case 'update':
				$this->update($_POST);
				$this->index();
			break;
			default:
				$this->index();
			break;
		}
	}

	public function __construct(){
		$this->model = new Masterdigm_Settings_PopUp;
	}
}

