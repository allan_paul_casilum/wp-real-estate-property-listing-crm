<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Masterdigm_Controller_Settings_Property extends Masterdigm_Init{
	protected static $instance = null;
	public $model_settings_property;
	public $tab_content = 'settings-property';
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {
		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	public function controller($action = ''){
		if( trim($action) == '' ){
			$action = $this->get_action();
		}

		switch($action){
			case 'update_property_settings':
				$this->update($_POST);
				$this->index();
			break;
			default:
				$this->index();
			break;
		}
	}

	public function update($post){
		$this->model_settings_property->search_property('u', $post['md_property_status']);
		$this->model_settings_property->property_title('u', $post['md_property_title']);
		$this->model_settings_property->bookaviewing_url('u', $post['bookaviewing_url']);
	}

	public function index(){
		$tab_content = $this->tab_content;
		$url = admin_url( 'admin.php?page=' . md_admin_current_page() . '&tab='.$tab_content.'' );
		$tab = $this->nav_tab();

		$template = 'settings';
		$default_tab_content = $template;
		if( !md_get_current_tab() ) {
			$default_tab_content = $tab_content;
		}
		$content_data['url'] = $url;
		$content_data['tab'] = $default_tab_content;

		$content_data['error'] = $this->get_input_error();
		$content_data['property_status'] = $this->model_settings_property->fields_status();
		$content_data['md_property_status'] = $this->model_settings_property->search_property('r');
		$content_data['property_title'] = $this->model_settings_property->show_default_property_name();
		$content_data['md_property_title'] = $this->model_settings_property->property_title('r');
		$content_data['md_bookaviewing_url'] = $this->model_settings_property->bookaviewing_url('r');
		require_once Masterdigm_Controller_Settings_Init::get_instance()->view();
	}

	public function __construct(){
		$this->model_settings_property = new Masterdigm_Settings_Property;
	}
}

