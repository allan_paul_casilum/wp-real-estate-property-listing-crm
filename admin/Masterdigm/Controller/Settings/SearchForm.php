<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Masterdigm_Controller_Settings_SearchForm extends Masterdigm_Init{
	protected static $instance = null;
	public $model;
	public $tab_content = 'settings-search-form';
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {
		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */
		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	public function index(){
		$tab_content = $this->tab_content;
		$url = admin_url( 'admin.php?page=' . md_admin_current_page() . '&tab='.$tab_content.'' );
		$tab = $this->nav_tab();

		$template = 'settings';
		$default_tab_content = $template;
		if( !md_get_current_tab() ) {
			$default_tab_content = $tab_content;
		}

		$content_data['url'] = $url;
		$content_data['tab'] = $default_tab_content;
		$content_data['md_price_range_ten_start'] = $this->model->md_price_range_ten_start('r');
		$content_data['md_price_range_ten_end'] = $this->model->md_price_range_ten_end('r');
		$content_data['md_price_range_ten_step'] = $this->model->md_price_range_ten_step('r');
		$content_data['md_price_range_hundred_start'] = $this->model->md_price_range_hundred_start('r');
		$content_data['md_price_range_hundred_end'] = $this->model->md_price_range_hundred_end('r');
		$content_data['md_price_range_hundred_step'] = $this->model->md_price_range_hundred_step('r');
		$content_data['md_price_range_million_start'] = $this->model->md_price_range_million_start('r');
		$content_data['md_price_range_million_end'] = $this->model->md_price_range_million_end('r');
		$content_data['md_price_range_million_step'] = $this->model->md_price_range_million_step('r');
		require_once Masterdigm_Controller_Settings_Init::get_instance()->view();
	}

	public function controller($action = ''){
		if( trim($action) == '' ){
			$action = $this->get_action();
		}
		switch($action){
			case 'update':
				$this->model->md_price_range_ten_start('u', $_POST['md_price_range_ten_start']);
				$this->model->md_price_range_ten_end('u', $_POST['md_price_range_ten_end']);
				$this->model->md_price_range_ten_step('u', $_POST['md_price_range_ten_step']);
				$this->model->md_price_range_hundred_start('u', $_POST['md_price_range_hundred_start']);
				$this->model->md_price_range_hundred_end('u', $_POST['md_price_range_hundred_end']);
				$this->model->md_price_range_hundred_step('u', $_POST['md_price_range_hundred_step']);
				$this->model->md_price_range_million_start('u', $_POST['md_price_range_million_start']);
				$this->model->md_price_range_million_end('u', $_POST['md_price_range_million_end']);
				$this->model->md_price_range_million_step('u', $_POST['md_price_range_million_step']);
				$this->index();
			break;
			default:
				$this->index();
			break;
		}
	}

	public function __construct(){
		$this->model = new Masterdigm_Settings_SearchForm;
	}
}

