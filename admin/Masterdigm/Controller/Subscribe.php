<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Masterdigm_Controller_Subscribe extends Masterdigm_Init{
	protected static $instance = null;
	public $model_subscribe;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function form(){
		$current_user = wp_get_current_user();
		$url_slug = get_admin_url().$this->url_slug();
		require_once md_admin_partials() . 'subscribe/forms.php';
	}

	public function validate_form(){
		$activate_code = get_option('masterdigm_subscribe_transaction_key');
		$url_slug = get_admin_url().$this->url_slug();
		require_once md_admin_partials() . 'subscribe/validate-forms.php';
	}

	public function index(){
		require_once md_admin_partials() . 'subscribe/setup-keys.php';
	}

	public function subscribe_api_key(){
		$email = '';
		$error 		= array();
		$has_error 	= false;
		if(
			isset($_POST['email'])
			&& is_email($_POST['email'])
			&& trim($_POST['email']) != ''
		){
			$email = $_POST['email'];
		}else{
			$has_error = true;
			$error[] = __('Please provide valid email address', md_localize_domain());
		}
		$company = '';
		if( isset($_POST['company']) ){
			$company = $_POST['company'];
		}
		$first_name = '';
		if( isset($_POST['first_name']) ){
			$first_name = $_POST['first_name'];
		}
		$last_name = '';
		if( isset($_POST['last_name']) ){
			$last_name = $_POST['last_name'];
		}

		if( $has_error ){
			$this->set_input_error($error);
			$this->index();
		}else{
			$arg_subscribe_data = array(
				'email' => $email,
				'company' => $company,
				'first_name' => $first_name,
				'last_name' => $last_name,
			);
			$this->model_subscribe->get_transaction_key($arg_subscribe_data);
			if(
				$this->model_subscribe->masterdigm_subscribe_step_1('r')
				&& $this->model_subscribe->masterdigm_subscribe_step_1('r') == 1
			){
				$arg_subscribe_data['k'] = $this->model_subscribe->masterdigm_subscribe_transaction_key('r');
				$this->model_subscribe->activate_transaction_key($arg_subscribe_data);
				if(
					$this->model_subscribe->masterdigm_subscribe_step_2('r')
					&& $this->model_subscribe->masterdigm_subscribe_step_2('r') == 1
				){
					$msg = $this->model_subscribe->masterdigm_subscribe_msg_step_2('r');
				}else{
					$has_error = true;
					$error[] = $this->model_subscribe->masterdigm_subscribe_msg_step_2('r');
					$this->set_input_error($error);
				}
			}else{
				$has_error = true;
				$error[] = __('Error in transaction key, please try again', PLUGIN_NAME);
				$this->set_input_error($error);
			}
		}
		$this->index();
	}

	public function activate_code(){
		$error 		= array();
		$has_error 	= false;
		$activate_error = false;
		$activation_code = '';

		$api = new Masterdigm_Settings_API;

		if(
			isset($_POST['activation_code'])
			&& $_POST['activation_code'] != ''
		){
			$activation_code = $_POST['activation_code'];
		}
		$arg = array(
			'activation_code' => $activation_code,
			'email' => $this->model_subscribe->masterdigm_subscribe_email_used('r')
		);

		if(
			$this->model_subscribe->masterdigm_subscribe_step_2('r')
			&& $this->model_subscribe->masterdigm_subscribe_step_2('r') == 1
		){
			$activate_code 	= $this->model_subscribe->validate_activation_code($arg);
			if( $activate_code ){
				$step_3 = $this->model_subscribe->masterdigm_subscribe_step_3('r');
				$step_3_data = $this->model_subscribe->masterdigm_subscribe_api_step_3('r');
				$step_3_activation_code = $this->model_subscribe->masterdigm_subscribe_activation_code('r');
				$step_3_who_activate = $this->model_subscribe->masterdigm_subscribe_step_3_current_user('r');

				//step 3
				if(
					$step_3
					&& $step_3 == 1
				){
					$data = $this->model_subscribe->masterdigm_subscribe_api_step_3('r');
					if( $data['data_result'] && $data['data_result']->result == 'success' ){
						$key = $data['data_result']->api_access->key;
						$api->key('u', $key);
						$token = $data['data_result']->api_access->token;
						$api->token('u', $token);
						$account_id = $data['data_result']->account_id;
						$api->broker_id('u', $account_id);
						$api->default_feed('u', 'crm');

						//auto add default settings setup
						$md_api = new Masterdigm_CRM_API();
						$client = $md_api->setCredentials(
							$api->key('r'),
							$api->token('r'),
							md_crm_api_endpoint(),
							md_crm_api_version()
						);
						$client->connect();
						//cache_clean();

						//auto create settings
						$settings_status = $client->get_fields();
						$key_active = 0;
						if( isset($settings_status->result) && $settings_status->result == 'success' ){
							foreach($settings_status->fields->status as $key => $val){
								if( strtolower($val) == 'active' ){
									$key_active = $key;
								}
							}
						}
						$property_settings = new Masterdigm_Settings_Property;
						$property_settings->search_property('u', $key_active);
						$property_settings->property_title('u', 'tagline');

						$client_lead_settings = new Masterdigm_Settings_ClientLead;
						$client_lead_settings->lead_status('u', 1);
						$client_lead_settings->lead_type('u', 1);
						//finish setup
						$this->model_subscribe->masterdigm_subscribe_api_finish('u', 1);
					}
				}
			}else{
				$activate_error = true;
				$has_error = true;
				$msg = $this->model_subscribe->masterdigm_subscribe_api_step_3_msg('r');
				$error[] = $msg;
				$this->set_input_error($error);
			}
		}
	}

	public function controller($action = ''){
		$model_subscribe = new Masterdigm_Subscribe;
		if( trim($action) == '' ){
			$action = $this->get_action();
		}
		switch($action){
			case 'subscribe_api_key':
				$this->subscribe_api_key();
			break;
			case 'activate_code':
				$this->activate_code();
				if(
					md_has_api_credentials() &&
					md_has_passed_prerequisite() &&
					md_finish_subscribe()
				){
					//welcome page
					$dashboard = new Masterdigm_Controller_Dashboard;
					$dashboard->controller();
				}else{
					$this->index();
				}
			break;
			default:
				$this->index();
			break;
		}
	}

	public function menu_slug(){
		return 'md-subscribe-api';
	}

	public function url_slug(){
		return 'admin.php?page=' . $this->menu_slug();
	}

	public function obj_model_subscribe(){
		$this->model_subscribe = new Masterdigm_Subscribe;
	}

	public function __construct(){
		$this->obj_model_subscribe();
	}
}
