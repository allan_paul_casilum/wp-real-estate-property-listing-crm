<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Masterdigm_Init{
	public $array_error = array();
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	public function nav_tab($tab_template = ''){
		$tab = new Masterdigm_View;
		return $tab->tab_nav($tab_template);
	}

	public function set_input_error($array_error){
		$this->array_error = $array_error;
	}

	public function get_input_error(){
		return $this->array_error;
	}

	public function get_action(){
		if( isset($_GET['action']) ){
			$action = $_GET['action'];
		}elseif( isset($_POST['action']) ){
			$action = $_POST['action'];
		}
		return sanitize_text_field($action);
	}

}
