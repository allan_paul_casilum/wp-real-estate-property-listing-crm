<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Masterdigm_View{
	protected static $instance = null;
	public $current_tab_nav = '';

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function get_current_tab_nav($tab = ''){
		if( isset($_GET['tab']) && $_GET['tab'] != '' ){
			return $_GET['tab'];
		}
		return false;
	}

	public function tab_nav($template = ''){
		$current_tab = $this->get_current_tab_nav();
		$admin_page_url = '';
		if( file_exists( md_admin_partials() . $template .'/'. $template . '-tab-nav.php' ) ){
			return md_admin_partials() . $template .'/'. $template . '-tab-nav.php';
		}
		return false;
	}

	public function __construct(){}
}
