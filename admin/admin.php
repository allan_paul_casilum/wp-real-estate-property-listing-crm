<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
// hook admin menu
add_action( 'admin_menu', 'register_md_menu_page' );
function register_md_menu_page(){
	//welcome page
	add_menu_page(
		'Masterdigm',
		'Masterdigm',
		'manage_options',
		md_plugin_name(),
		array(Masterdigm_Controller_Dashboard::get_instance(), 'controller'),
		'',
		21
	);
	if( md_has_api_credentials() ){
		//settings submenu
		add_submenu_page(
			md_plugin_name(),
			'Settings',
			'Settings',
			'manage_options',
			Masterdigm_Settings_Init::get_instance()->menu_slug(),
			array(Masterdigm_Controller_Settings_API::get_instance(), 'controller')
		);
	}
	//settings api - hide menu
	add_submenu_page(
		md_plugin_name(),
		'API Settings',
		'API Settings',
		'manage_options',
		Masterdigm_Settings_API::get_instance()->menu_slug(),
		array(Masterdigm_Controller_Settings_API::get_instance(), 'controller')
	);
	//settings property - hide menu
	add_submenu_page(
		md_plugin_name(),
		'Property Settings',
		'Property Settings',
		'manage_options',
		Masterdigm_Settings_Property::get_instance()->menu_slug(),
		array(Masterdigm_Controller_Settings_Property::get_instance(), 'controller')
	);
	add_submenu_page(
		md_plugin_name(),
		'Search Form Settings',
		'Search Form Settings',
		'manage_options',
		Masterdigm_Settings_SearchForm::get_instance()->menu_slug(),
		array(Masterdigm_Controller_Settings_SearchForm::get_instance(), 'controller')
	);
	add_submenu_page(
		md_plugin_name(),
		'Client Lead Settings',
		'Client Lead Settings',
		'manage_options',
		Masterdigm_Settings_ClientLead::get_instance()->menu_slug(),
		array(Masterdigm_Controller_Settings_ClientLead::get_instance(), 'controller')
	);
	add_submenu_page(
		md_plugin_name(),
		'Mail Settings',
		'Mail Settings',
		'manage_options',
		Masterdigm_Settings_Mail::get_instance()->menu_slug(),
		array(Masterdigm_Controller_Settings_Mail::get_instance(), 'controller')
	);
	add_submenu_page(
		md_plugin_name(),
		'Pop-Up Settings',
		'Pop-Up Settings',
		'manage_options',
		Masterdigm_Settings_PopUp::get_instance()->menu_slug(),
		array(Masterdigm_Controller_Settings_Popup::get_instance(), 'controller')
	);
	//settings subscribe - hide menu
	add_submenu_page(
		null,
		'Subscribe',
		'Subscribe',
		'manage_options',
		Masterdigm_Controller_Subscribe::get_instance()->menu_slug(),
		array(Masterdigm_Controller_Subscribe::get_instance(), 'controller')
	);
}
function md_admin_head() {
    remove_submenu_page(
        md_plugin_name(),
		Masterdigm_Settings_API::get_instance()->menu_slug()
    );
    remove_submenu_page(
        md_plugin_name(),
		Masterdigm_Settings_Property::get_instance()->menu_slug()
    );
    remove_submenu_page(
        md_plugin_name(),
		Masterdigm_Settings_SearchForm::get_instance()->menu_slug()
    );
    remove_submenu_page(
        md_plugin_name(),
		Masterdigm_Settings_ClientLead::get_instance()->menu_slug()
    );
    remove_submenu_page(
        md_plugin_name(),
		Masterdigm_Settings_Mail::get_instance()->menu_slug()
    );
    remove_submenu_page(
        md_plugin_name(),
		Masterdigm_Settings_PopUp::get_instance()->menu_slug()
    );
}
add_action( 'admin_head', 'md_admin_head' );
function md_check_default_page(){
	return array(
		'Property',
		'Search Properties',
		'My Account',
		'State',
		'County',
		'City',
		'Community',
		'Un Subscribe',
	);
}
function md_check_permalinks_enable(){
	return get_option('permalink_structure');
}
require_once 'md-admin-partials-view.php';
