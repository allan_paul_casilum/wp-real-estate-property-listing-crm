<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$admin_view = new Masterdigm_View;
function md_tab_nav($tab_template = ''){
	global $admin_view;
	$tab = $admin_view;
	if( $tab->tab_nav($tab_template) ){
		$current_tab = $tab->get_current_tab_nav();
		require_once $tab->tab_nav($tab_template);
	}
}
function md_tab_content($content_template, $default_tab_content = '', $content_data = array()){
	global $admin_view;

	$current_tab = $admin_view;
	if( $default_tab_content == '' ){
		$default_tab_content = $content_template;
	}
	$template = md_admin_partials() . $content_template . '/' . $default_tab_content . '.php';
	if(	$current_tab->get_current_tab_nav() ){
		$template = md_admin_partials() . $content_template . '/' . $current_tab->get_current_tab_nav() . '.php';
	}
	if( file_exists($template) ){
		extract($content_data);
		require_once $template;
	}
}
function md_get_current_tab(){
	global $admin_view;
	return $admin_view->get_current_tab_nav();
}
function md_admin_current_page(){
	$admin_page_url = '';
	if( isset($_GET['page']) ){
		$admin_page_url = $_GET['page'];
	}
	return $admin_page_url;
}
function md_admin_get_error($error = array()){
	if(is_array($error) && count($error) > 0){
		$template = md_admin_partials() . 'input-error.php';
		require_once $template;
	}
}
