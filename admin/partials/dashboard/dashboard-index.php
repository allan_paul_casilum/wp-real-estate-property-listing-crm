<div class="wrap about-wrap">
	<h1>
		<?php _e('Welcome to ', md_localize_domain()); ?>
		<?php echo esc_html( get_admin_page_title() ); ?>
		<?php echo md_plugin_version();?>
	</h1>

	<div class="about-text">
		<h3>
			<?php _e('An All-in-One Real Estate Plugin Solution for Brokerages and Agents Integrated with a High Powered Real Estate CRM Solution.', md_localize_domain());?>
		</h3>
	</div>
	<?php md_tab_nav($template); ?>
	<?php md_tab_content($template);  ?>
	<div class="about-text">
		<a href="https://wordpress.org/support/view/plugin-reviews/wp-real-estate-property-listing-crm" target="_blank">
			<img src="<?php //echo PLUGIN_ASSET_URL . 'rate-us.jpg';?>" alt="rate us"/>
		</a>
	</div>
</div>
