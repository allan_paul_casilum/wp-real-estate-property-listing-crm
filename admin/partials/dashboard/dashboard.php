<div class="feature-section two-col">
	<div class="col">
		<h3 style="color:#0d3965;"><?php _e('Hello', md_localize_domain()); ?></h3>
		<p>
		<?php _e('Welcome to Masterdigm!  A full Real Estate plugin highly integrated with Masterdigm real estate CRM!', md_localize_domain()); ?>
		</br>
		<?php _e('You will notice several tabs above, i.e., CRM, Webinars, etc.. That will guide you through the Masterdigm processes.', md_localize_domain()); ?>
		</br>
		<?php _e('We want to see you succeed as a Broker, Agent or Web Development Team!  Therefore, we have a "support" tab to connect with us to ensure your success!', md_localize_domain()); ?>
		</br>
		<?php _e('Cheers, and we look forward to hearing from you.', md_localize_domain()); ?>
		</br>
		</br>
		Team Masterdigm<br>800-982-1276
		</p>
		<h3><?php _e('Plugin is fully setup', md_localize_domain()); ?></h3>
		<hr>
		<h3><?php _e('Setup Mail', md_localize_domain()); ?></h3>
		<p><a href="<?php //echo admin_url( \Settings_API::get_instance()->get_slug() ); ?>#tabs-mail"><?php _e('Setup mail settings to update subscription content', md_localize_domain()); ?></a></p>
		<hr>
		<h3><?php _e('Setup Lead', md_localize_domain()); ?></h3>
		<p><a href="<?php //echo admin_url( \Settings_API::get_instance()->get_slug() ); ?>#tabs-client-lead"><?php _e('Setup lead status and type', md_localize_domain()); ?></a></p>
		<hr>
		<h3><?php _e('See Properties', md_localize_domain()); ?></h3>
		<?php
			/*if( get_option('md_finish_install') ){
				$log = \Create_Location_Page::get_instance()->get_option_log();
				echo "<p><a href='{$search_properties}'>".__('Click to see properties!', md_localize_domain())."</a></p>";
			}*/
		?>
	</div>
	<div class="col">
		<h3 style="color:#0d3965;"><?php _e('Watch this video first!', md_localize_domain()); ?></h3>
		<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLavLSlMVm_F970AGFX2Bk3tZIfP7l97dd" frameborder="0" allowfullscreen></iframe>
	</div>
</div>
<hr>
<div class="changelog">
	<h3>What's New in Masterdigm CRM and MD Plugin</h3>
	<?php //require_once $update_notice; ?>
</div>
<hr>
<div class="changelog point-releases">
	<h3 class="warning" style="color:red;"><?php _e('PLEASE NOTE | CAUTION: Do not delete the following pages OR the Masterdigm plugin will not work!', md_localize_domain());?></h3>
	<ul>
		<li><?php _e('Property - display the single or full details of property.', md_localize_domain());?></li>
		<li><?php _e('Search Properties - Display the result of the searched properties', md_localize_domain());?></li>
		<li><?php _e('City - Display list of properties base in City', md_localize_domain());?></li>
		<li><?php _e('State - Display list of properties base in State', md_localize_domain());?></li>
		<li><?php _e('Community - Display list of properties base in Community', md_localize_domain());?></li>
		<li><?php _e('County - Display list of properties base in County', md_localize_domain());?></li>
	</ul>
	<p></p>
</div>
<div class="feature-section">
	<h3><?php _e('And you want to know more of our plugin and CRM?', md_localize_domain());?></h3>
	<p><?php _e('Want to discuss anything else? We offer a free 30 minute Gotomeeting screen-share where we go into answering questions you may have.', md_localize_domain());?></p>
	<iframe width="100%" height="500" src="https://www.vcita.com/v/masterdigmcrm/online_scheduling?service_id=59f30ff5&staff_id=dcdf8984#/schedule" frameborder="0" allowfullscreen></iframe>
</div>
