<div class="about-text">
	<div class="feature-section two-col">
		<div class="col">
			<h3>FAQ</h3>
			<p><?php _e('Stuck?  Have an issue with Masterdigm or its premium plugins?', md_localize_domain());?></p>
			<p><a href="http://www.masterdigm.com/faq/" target="_blank"><?php _e('Click here and you will be redirected to our FAQ page.', md_localize_domain());?></a></p>
		</div>
		<div class="col">
			<h3><?php _e('Any other issues?', md_localize_domain());?></h3>
			<p><?php _e('You have many other ways to connect with us.', md_localize_domain());?></p>
			<p>a.) <?php _e('Zopim live chat within the CRM', md_localize_domain());?></p>
			<p>b.) <?php _e('Vcita: on our home page.. Set up a meeting', md_localize_domain());?></p>
			<p>c.) <?php _e('Support tab inside the CRM.', md_localize_domain());?></p>
		</div>
	</div>
</div>
