<?php md_admin_get_error($error); ?>
<?php md_tab_nav($template); ?>
<form name="md_api" method="post" action="<?php echo $url; ?>">
	<input type="hidden" name="action" value="update">
	<input type="hidden" name="tab" value="<?php echo $tab;?>">
	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row">
					<label for="md_lead_status"><?php _e('Lead Status', md_localize_domain());?></label>
				</th>
				<td>
					<p>
						<?php _e('Choose lead status', md_localize_domain());?>,
						<?php _e('This will be the status of the lead in the CRM', md_localize_domain());?>
					</p>
					<select name="md_lead_status">
						<?php foreach($md_lead_status as $key => $val){ ?>
								<option value="<?php echo $key;?>" <?php echo ( $model_lead->lead_status('r') == $key ) ? 'selected':'';?>><?php echo $val;?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="md_lead_type"><?php _e('Lead Type', md_localize_domain());?></label>
				</th>
				<td>
					<p>
						<?php _e('Choose lead type', PLUGIN_NAME);?>,
						<?php _e('This will be the type of the lead in the CRM', PLUGIN_NAME);?>
					</p>
					<select name="md_lead_type">
						<?php foreach($md_lead_type as $key => $val){ ?>
								<option value="<?php echo $key;?>" <?php echo ( $model_lead->lead_type('r') == $key ) ? 'selected':'';?>><?php echo $val;?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
		</tbody>
	</table>
	<p class="submit">
		<input type="submit" name="Submit" class="button-primary" value="<?php _e('Update', md_localize_domain()) ?>" />
	</p>
</form>
