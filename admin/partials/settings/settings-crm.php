<?php md_admin_get_error($error); ?>
<form name="md_api" method="post" action="<?php echo $url; ?>">
	<input type="hidden" name="action" value="update_api">
	<input type="hidden" name="tab" value="<?php echo $tab;?>">
	<h3>API key</h3>
	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row"><label for="settings_api_default_feed"><?php _e('Property Data Feed', md_localize_domain());?></label></th>
				<td>
					<select name="settings_api_default_feed">
						<option value="0"><?php _e('Select Data Feed', md_localize_domain());?></option>
						<option value="crm" <?php echo ($settings_api_default_feed == 'crm') ? 'selected':''; ?>>CRM</option>
						<option value="mls" <?php echo ($settings_api_default_feed == 'mls') ? 'selected':''; ?>>MLS</option>
					</select>
					<p><?php _e('Search Property, Choose which default data to get properties feed, mostly the default is CRM ( unless masterdigm setup a MLS data on your account )', md_localize_domain());?></p>
				</td>
			</tr>
			<tr>
				<th scope="row"><label for="settings_api_key">API KEY : </label></th>
				<td><input type="text" name="settings_api_key" value="<?php echo $settings_api_key;?>" style="width:100%;"></td>
			</tr>
			<tr>
				<th scope="row"><label for="settings_api_token">API TOKEN : </label></th>
				<td><input type="text" name="settings_api_token" value="<?php echo $settings_api_token;?>" style="width:100%;"></td>
			</tr>
			<tr>
				<th scope="row"><label for="settings_api_broker_id">Broker ID : </label></th>
				<td><input type="text" name="settings_api_broker_id" value="<?php echo $settings_api_broker_id;?>" style="width:100%;"></td>
			</tr>
		</tbody>
	</table>
	<p class="submit">
		<input type="submit" name="Submit" class="button-primary" value="<?php _e('Update', md_localize_domain()) ?>" />
	</p>
</form>
