<?php md_admin_get_error($error); ?>
<?php md_tab_nav($template); ?>
<form name="md_api" method="post" action="<?php echo $url; ?>">
	<input type="hidden" name="action" value="update">
	<input type="hidden" name="tab" value="<?php echo $tab;?>">
	<p class="submit">
		<input type="submit" name="Submit" class="button-primary" value="<?php _e('Update', md_localize_domain()) ?>" />
	</p>
	<h3><?php _e('Mail Content', md_localize_domain());?></h3>
	<p><?php _e('Outgoing email.  Use this mail system to send an email from the mail server', md_localize_domain());?></p>
	<input type="text" name="md_settings_mail_server" value="<?php echo $md_settings_mail_server;?>" style="width:100%;">
	<p>
		<?php _e('The email content below is the email the user receives upon subscribing.  Be sure to include the string variable, e.g., %username% and %password% so they will receive their log in credentials.', md_localize_domain()); ?>
	</p>
	<div>
		<p><?php _e('Use the below string variables to create your email below.', md_localize_domain());?></p>
		<ul>
			<li>%name% - <?php _e('The name of the registered user', md_localize_domain());?></li>
			<li>%username% - <?php _e('The user-name of the registered user', md_localize_domain());?></li>
			<li>%password% - <?php _e('The password of the registered user', md_localize_domain());?></li>
			<li>%email% - <?php _e('The email of the registered user', md_localize_domain());?></li>
			<li>%sitename% - <?php _e('The name the current website', md_localize_domain());?></li>
			<li>%loginurl% - <?php _e('The url of the current website', md_localize_domain());?></li>
		</ul>
	</div>
	<p><?php _e('Subject', md_localize_domain());?></p>
	<input type="text" name="md_settings_mail_subject" value="<?php echo $subject;?>" style="width:100%;">
	<p><?php _e('Message', md_localize_domain());?></p>
	<?php wp_editor( $content, $editor_id, $settings ); ?>
	<p class="submit">
		<input type="submit" name="Submit" class="button-primary" value="<?php _e('Update', md_localize_domain()) ?>" />
	</p>
</form>
