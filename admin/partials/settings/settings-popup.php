<?php md_admin_get_error($error); ?>
<?php md_tab_nav($template); ?>
<form name="md_api" method="post" action="<?php echo $url; ?>">
	<input type="hidden" name="action" value="update">
	<input type="hidden" name="tab" value="<?php echo $tab;?>">
	<h3><?php _e('Show pop-up on certain clicks?', md_localize_domain());?></h3>
	<p>
		<?php
			_e('How this works? when a visitor visit your website, ', md_localize_domain());
			_e('if \'turn on\' it will display after certain click of property pages', md_localize_domain());
			_e('these are the individual property page. The popup will force the visitor to register to continue browsing.', md_localize_domain());
		?>
	</p>
	<select name="md_popup_show">
		<?php foreach($show_popup_choose as $key=>$val){ ?>
				<option value="<?php echo $key;?>" <?php echo ($md_popup_show == $key) ? 'selected':'';?>><?php echo $val;?></option>
		<?php } ?>
	</select>
	<p>
		<?php _e('Display close button? do you want the ability to close the popup?', md_localize_domain());?>
	</p>
	<select name="md_popup_close">
		<?php foreach($show_popup_close_button as $key=>$val){ ?>
				<option value="<?php echo $key;?>" <?php echo ($md_popup_close == $key) ? 'selected':'';?>><?php echo $val;?></option>
		<?php } ?>
	</select>
	<p>
		<?php _e('Show popup after certain clicks. This will show popup after certain click or view of individual property', md_localize_domain()); ?>
	</p>
	<select name="md_popup_clicks">
		<?php foreach($show_popup_after as $key=>$val){ ?>
				<option value="<?php echo $key;?>" <?php echo ( $md_popup_clicks == $key ) ? 'selected':'';?>><?php echo $val;?></option>
		<?php } ?>
	</select>
	<p class="submit">
		<input type="submit" name="Submit" class="button-primary" value="<?php _e('Update', md_localize_domain()) ?>" />
	</p>
</form>
