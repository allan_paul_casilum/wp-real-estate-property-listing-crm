<?php md_admin_get_error($error); ?>
<?php md_tab_nav($template); ?>
<form name="md_api" method="post" action="<?php echo $url; ?>">
	<input type="hidden" name="action" value="update_property_settings">
	<input type="hidden" name="tab" value="<?php echo $tab;?>">
	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row">
					<label for="md_property_title"><?php _e('Default Search', md_localize_domain());?></label>
				</th>
				<td>
					<p><?php _e('Search property by default search criteria, Default status, when visitor search property, they will see this search criteria status', md_localize_domain());?></p>
					<select name="md_property_status">
						<?php foreach($property_status as $key => $val){ ?>
								<option value="<?php echo $key;?>" <?php echo ( $md_property_status == $key ) ? 'selected':'';?>><?php echo $val;?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="md_property_title"><?php _e('Property Title', md_localize_domain());?></label>
				</th>
				<td>
					<p><?php _e('Display property address or tag-line', md_localize_domain());?></p>
					<select name="md_property_title">
						<?php foreach($property_title as $key => $val){ ?>
								<option value="<?php echo $key;?>" <?php echo ( $md_property_title == $key ) ? 'selected':'';?>><?php echo $val;?></option>
						<?php } ?>
					</select>
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="md_property_title"><?php _e('Book a viewing URL', md_localize_domain());?></label>
				</th>
				<td>
					<input type="text" name="bookaviewing_url" style="width:60%;" value="<?php echo $md_bookaviewing_url;?>">
				</td>
			</tr>
		</tbody>
	</table>
	<p class="submit">
		<input type="submit" name="Submit" class="button-primary" value="<?php _e('Update', md_localize_domain()) ?>" />
	</p>
</form>
