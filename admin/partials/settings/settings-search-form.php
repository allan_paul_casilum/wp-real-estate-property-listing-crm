<?php md_admin_get_error($error); ?>
<?php md_tab_nav($template); ?>
<form name="md_api" method="post" action="<?php echo $url; ?>">
	<input type="hidden" name="action" value="update">
	<input type="hidden" name="tab" value="<?php echo $tab;?>">
	<h2>Price Range</h2>
	<hr>
	<h3>Change price range by Tens</h3>
	<p><span style="font-style:italic;color:red;">(<?php _e('if you want the by Tens not to show, put zero in start, end and step', md_localize_domain());?>)</span></p>
	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row">
					<label for="md_price_range_ten_start"><?php _e('Start', md_localize_domain());?></label>
				</th>
				<td>
					<input type="text" name="md_price_range_ten_start" value="<?php echo $md_price_range_ten_start;?>">
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="md_price_range_ten_end"><?php _e('End', md_localize_domain());?></label>
				</th>
				<td>
					<input type="text" name="md_price_range_ten_end" value="<?php echo $md_price_range_ten_end;?>">
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="md_price_range_ten_step"><?php _e('Step', md_localize_domain());?></label>
				</th>
				<td>
					<p>Increment by</p>
					<input type="text" name="md_price_range_ten_step" value="<?php echo $md_price_range_ten_step;?>">
				</td>
			</tr>
			<tr>
				<th scope="row">
				</th>
				<td>
					<p class="submit">
						<input type="submit" name="Submit" class="button-primary" value="<?php _e('Update', md_localize_domain()) ?>" />
					</p>
				</td>
			</tr>
		</tbody>
	</table>
	<hr>
	<h3>Change price range by Hundred</h3>
	<p><span style="font-style:italic;color:red;">(if you want the by Hundred not to show, put zero in start, end and step)</span></p>
	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row">
					<label for="md_price_range_hundred_start"><?php _e('Start', md_localize_domain());?></label>
				</th>
				<td>
					<input type="text" name="md_price_range_hundred_start" value="<?php echo $md_price_range_hundred_start;?>">
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="md_price_range_hundred_end"><?php _e('End', md_localize_domain());?></label>
				</th>
				<td>
					<input type="text" name="md_price_range_hundred_end" value="<?php echo $md_price_range_hundred_end;?>">
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="md_price_range_hundred_step"><?php _e('Step', md_localize_domain());?></label>
				</th>
				<td>
					<p>Increment by</p>
					<input type="text" name="md_price_range_hundred_step" value="<?php echo $md_price_range_hundred_step;?>">
				</td>
			</tr>
			<tr>
				<th scope="row">
				</th>
				<td>
					<p class="submit">
						<input type="submit" name="Submit" class="button-primary" value="<?php _e('Update', md_localize_domain()) ?>" />
					</p>
				</td>
			</tr>
		</tbody>
	</table>
	<hr>
	<h3>Change price range by Million</h3>
	<p><span style="font-style:italic;color:red;">(if you want the by Million not to show, put zero in start, end and step)</span></p>
	<table class="form-table">
		<tbody>
			<tr>
				<th scope="row">
					<label for="md_price_range_million_start"><?php _e('Start', md_localize_domain());?></label>
				</th>
				<td>
					<input type="text" name="md_price_range_million_start" value="<?php echo $md_price_range_million_start;?>">
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="md_price_range_million_end"><?php _e('End', md_localize_domain());?></label>
				</th>
				<td>
					<input type="text" name="md_price_range_million_end" value="<?php echo $md_price_range_million_end;?>">
				</td>
			</tr>
			<tr>
				<th scope="row">
					<label for="md_price_range_million_step"><?php _e('Step', md_localize_domain());?></label>
				</th>
				<td>
					<p>Increment by</p>
					<input type="text" name="md_price_range_million_step" value="<?php echo $md_price_range_million_step;?>">
				</td>
			</tr>
			<tr>
				<th scope="row">
				</th>
				<td>
					<p class="submit">
						<input type="submit" name="Submit" class="button-primary" value="<?php _e('Update', md_localize_domain()) ?>" />
					</p>
				</td>
			</tr>
		</tbody>
	</table>
</form>
