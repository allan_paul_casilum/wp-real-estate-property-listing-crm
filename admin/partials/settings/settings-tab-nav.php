<h2 class="nav-tab-wrapper">
	<a href="<?php echo admin_url( 'admin.php?page=' . Masterdigm_Settings_API::get_instance()->menu_slug() . '&tab=settings-crm' );?>" class="nav-tab <?php echo ($current_tab == 'settings-crm' || $current_tab == '') ? 'nav-tab-active':'';?>">CRM</a>
	<a href="<?php echo admin_url( 'admin.php?page=' . Masterdigm_Settings_Property::get_instance()->menu_slug() . '&tab=settings-property' );?>" class="nav-tab <?php echo ($current_tab == 'settings-property') ? 'nav-tab-active':'';?>">Property</a>
	<a href="<?php echo admin_url( 'admin.php?page=' . Masterdigm_Settings_SearchForm::get_instance()->menu_slug() . '&tab=settings-search-form' );?>" class="nav-tab <?php echo ($current_tab == 'settings-search-form') ? 'nav-tab-active':'';?>">Search Form</a>
	<a href="<?php echo admin_url( 'admin.php?page=' . Masterdigm_Settings_ClientLead::get_instance()->menu_slug() . '&tab=settings-client-lead' );?>" class="nav-tab <?php echo ($current_tab == 'settings-client-lead') ? 'nav-tab-active':'';?>">Client Lead</a>
	<a href="<?php echo admin_url( 'admin.php?page=' . Masterdigm_Settings_Mail::get_instance()->menu_slug() . '&tab=settings-mail' );?>" class="nav-tab <?php echo ($current_tab == 'settings-mail') ? 'nav-tab-active':'';?>">Mail</a>
	<a href="<?php echo admin_url( 'admin.php?page=' . Masterdigm_Settings_PopUp::get_instance()->menu_slug() . '&tab=settings-popup' );?>" class="nav-tab <?php echo ($current_tab == 'settings-popup') ? 'nav-tab-active':'';?>">Popup</a>
</h2>
