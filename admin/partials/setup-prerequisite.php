<div class="wrap about-wrap">
	<h1>
		<?php _e('Welcome to', md_localize_domain());?> <?php echo esc_html( get_admin_page_title() ); ?>
		<?php echo $plugin->get_version();?>
	</h1>
	<div class="about-text">
		<p><?php _e('Thank you for choosing Masterdigm CRM as your property solutions', md_localize_domain());?></p>
	</div>
	<div class="changelog point-releases">
		<h2><?php _e('Before signing-up for API Keys, we would like you to check on these issues below and please fix them :', PLUGIN_NAME);?></h2>
		<?php if( $cache_folder ){ ?>
				<?php foreach($cache_folder as $val) { ?>
					<p style="font-size:18px;color:red;"><?php echo $val?></p>
				<?php } ?>
				<p style="font-style:italic;"><?php _e('The upload folder is needed so the cache can put data in there', PLUGIN_NAME);?></p>
				<hr>
		<?php } ?>
		<?php if( $permalinks ){ ?>
				<?php foreach($permalinks as $val) { ?>
					<p style="font-size:18px;color:red;"><?php echo $val?></p>
				<?php } ?>
				<p style="font-style:italic;"><?php _e('The permalinks, need to setup so the url will work in property pages', PLUGIN_NAME);?></p>
				<hr>
		<?php } ?>
		<?php if( $pages ){ ?>
				<?php foreach($pages as $val) { ?>
					<p style="font-size:18px;color:red;"><?php echo $val?></p>
				<?php } ?>
			<p style="font-style:italic;"><?php _e('Those pages are needed to display property data from CRM, those hold as placeholder and contain shortcodes', PLUGIN_NAME);?></p>
		<?php } ?>
	</div>
	<form name="md_api" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
		<input type="submit" name="Submit" class="button-primary" value="<?php _e('Refresh', PLUGIN_NAME ) ?>" />
	</form>
</div>
