<div class="wrap">
	<h1><?php _e('Welcome to', md_localize_domain());?> Masterdigm <?php echo md_plugin_version();?></h1>
	<div class="about-wrap">
		<div>
			<?php if($this->get_input_error()){ ?>
				<div class="error">
					<ul>
						<?php foreach($this->get_input_error() as $val) { ?>
								<li><p class="error"><?php echo $val;?></p></li>
						<?php } ?>
					</ul>
				</div>
			<?php } ?>
		</div>
		<div class="feature-section two-col">
			<div class="col">
				<?php if( $this->model_subscribe->masterdigm_subscribe_step_2('r') && $this->model_subscribe->masterdigm_subscribe_step_2('r') == 1){ ?>
					<?php Masterdigm_Controller_Subscribe::get_instance()->validate_form(); ?>
				<?php } ?>
				<?php Masterdigm_Controller_Subscribe::get_instance()->form(); ?>
			</div>
			<div class="col">
				<?php Masterdigm_Controller_Settings_API::get_instance()->form(); ?>
			</div>
		</div><!-- feature-section -->
	</div><!-- about wrap -->
</div>

