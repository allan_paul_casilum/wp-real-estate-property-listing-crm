<h3><?php _e('Validate Code', PLUGIN_NAME);?></h3>
<?php if( $this->model_subscribe->masterdigm_subscribe_msg_step_2('r') ){ ?>
	<p><?php echo $this->model_subscribe->masterdigm_subscribe_msg_step_2('r'); ?></p>
<?php } ?>
<form name="md_api" method="post" action="<?php echo $url_slug;?>">
	<input type="hidden" name="action" value="activate_code">
	<table class="form-table">
		<tbody>
			<tr>
				<td>
					<input type="text" name="activation_code" value="" style="width:100%;">
				</td>
			</tr>
		</tbody>
	</table>
	<p class="submit">
		<input type="submit" name="Submit" class="button-primary" value="<?php _e('Activate Code', 'activate_code' ) ?>" />
	</p>
</form>
<hr>
