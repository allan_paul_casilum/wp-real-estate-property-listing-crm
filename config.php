<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
function md_plugin_folder_name(){
	$plugin_foldername = explode('/',plugin_basename( __FILE__ ));
	return $plugin_foldername[0];
}
$md_config = array();
//wp related settings
$md_config['wp'] = array(
	'version' => '0',
	'plugin_name' => 'masterdigm',
	'localize_domain' => 'masterdigm',
	'plugin_version' => '3.33.16',
);
//md api
$md_config['api'] = array(
	'active_option_prefix' => 'md_plugin_activate',
	'md_api_endpoint' => 'http://masterdigm.com/api2/',
	'md_api_version' => 'v2',
);
$md_config['api_option_db'] = array(
	'token' => 'md_api_token',
	'key' => 'md_api_key',
	'broker_id' => 'md_api_broker_id',
	'feed' => 'md_api_default_property_feed',
);
//default page created
$md_config['pages'] = array(
	'property_detail' => get_option('md_property_detail_page'),
	'search_property_result' => get_option('md_search_property_result'),
);
//md template
$md_config['template'] = array(
	'plugin_dir_path' => plugin_dir_path( __FILE__ ),
	'plugin_folder_name' => md_plugin_folder_name(),
	'public_partials' => plugin_dir_path( __FILE__ ) .  'public/partials/',
	'admin_dir' => plugin_dir_path( __FILE__ ) .  'admin/',
	'admin_dir_partials' => plugin_dir_path( __FILE__ ) .  'admin/partials/',
);
$md_config['price_range'] = array(
	'ten' => array(
		'start' => 10000,
		'end' => 90000,
		'step' => 10000,
	),
	'hundred' => array(
		'start' => 100000,
		'end' => 900000,
		'step' => 25000,
	),
	'million' => array(
		'start' => 1000000,
		'end' => 9000000,
		'step' => 250000,
	),
);
function md_plugin_name(){
	global $md_config;
	return $md_config['wp']['plugin_name'];
}
function md_localize_domain(){
	global $md_config;
	return $md_config['wp']['localize_domain'];
}
function md_crm_api_endpoint(){
	global $md_config;
	return $md_config['api']['md_api_endpoint'];
}
function md_crm_api_version(){
	global $md_config;
	return $md_config['api']['md_api_version'];
}
function md_plugin_version(){
	global $md_config;
	return $md_config['wp']['plugin_version'];
}
function md_dir_admin($path = ''){
	global $md_config;
	if( trim($path) != '' ){
		return $md_config['template'][$path];
	}
}
function md_admin_partials(){
	global $md_config;
	return $md_config['template']['admin_dir_partials'];
}
function md_public_partials(){
	global $md_config;
	return $md_config['template']['public_partials'];
}
function md_price_range(){
	global $md_config;
	return $md_config['price_range'];
}
function md_price_range_by($by, $key){
	global $md_config;
	if( isset($md_config['price_range'][$by][$key]) ){
		return $md_config['price_range'][$by][$key];
	}
	return false;
}
function md_is_wp_upload_exists(){
	// check if media upload exists
	// this to make sure cache works
	$upload_dir = wp_upload_dir();
	if(
		file_exists($upload_dir['basedir'])
		&& is_writable($upload_dir['basedir'])
	){
		return true;
	}
	return false;
}
//========================================//
function md_dump($array, $exit = false){
	echo '<pre>';
		print_r($array);
	echo '</pre>';
	if( $exit ){
		exit();
	}
}
