<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Masterdigm_InstallNotice{
	protected static $instance = null;
	public $md_setup_finish = false;
	public $md_setup_prerequisite = false;
	public $md_setup_api = false;
	public $md_setup_settings = false;
	public $md_current_setup = false;
	public $has_passed_prerequisite = 'md_has_passed_prerequisite';
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function has_passed_prerequisite($action = '', $value = ''){
		$prefix = $this->has_passed_prerequisite;
		switch($action){
			case 'r':
				return get_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'd':
				delete_option($prefix);
			break;
		}
	}

	public function setup_prerequisite(){
		$step_one_pass = true;
		//check cache folder
		$need_setup = array();
		if( !md_is_wp_upload_exists() ){
			$need_setup['folder'][] = __("Folder wp-content/uploads doesn't exists, please create and make it writable", PLUGIN_NAME);
			$step_one_pass = false;
		}
		//check permalinks
		if( !md_check_permalinks_enable() ){
			$need_setup['permalinks'][] = __("The permalinks, need to set up so the url will work in property pages", PLUGIN_NAME);
			$step_one_pass = false;
		}
		//check pages
		foreach(md_check_default_page() as $pages){
			if( get_page_by_title($pages) ){
				$need_setup['wp_page'][] = '"'.$pages.'"' . __(" page already exists, please check or rename the page title and the url", PLUGIN_NAME);
			}
		}
		if( isset($need_setup['wp_page']) && count($need_setup['wp_page']) > 0 ){
			$step_one_pass = false;
		}

		if( $step_one_pass ){
			return true;
		}else{
			return $need_setup;
		}
	}

	public function __construct(){}
}
