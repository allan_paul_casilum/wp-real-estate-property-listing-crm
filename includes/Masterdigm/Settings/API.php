<?php
/**
 *
 * */
class Masterdigm_Settings_API {
	protected static $instance = null;
	public $config_api_options;
	public $has_credentials_key = false;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {
		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function config_api_options(){
		global $md_config;
		$this->config_api_options = $md_config['api_option_db'];
	}

	/**
	 * action
	 * c = create
	 * r = read
	 * u = update
	 * d = delete
	 * */
	public function token($action = '', $data = null){
		$name = $this->config_api_options['token'];
		switch($action){
			case 'c':
				$this->add($name, $data);
			break;
			case 'r':
				return $this->get($name);
			break;
			case 'u':
				$this->update($name, $data);
			break;
			case 'd':
				$this->delete($name);
			break;
		}
	}
	/**
	 * action
	 * c = create
	 * r = read
	 * u = update
	 * d = delete
	 * */
	public function key($action = '', $data = null){
		$name = $this->config_api_options['key'];
		switch($action){
			case 'c':
				$this->add($name, $data);
			break;
			case 'r':
				return $this->get($name);
			break;
			case 'u':
				$this->update($name, $data);
			break;
			case 'd':
				$this->delete($name);
			break;
		}
	}
	/**
	 * action
	 * c = create
	 * r = read
	 * u = update
	 * d = delete
	 * */
	public function broker_id($action = '', $data = null){
		$name = $this->config_api_options['broker_id'];
		switch($action){
			case 'c':
				$this->add($name, $data);
			break;
			case 'r':
				return $this->get($name);
			break;
			case 'u':
				$this->update($name, $data);
			break;
			case 'd':
				$this->delete($name);
			break;
		}
	}
	/**
	 * action
	 * c = create
	 * r = read
	 * u = update
	 * d = delete
	 * */
	public function default_feed($action = '', $data = null){
		$name = $this->config_api_options['feed'];

		switch($action){
			case 'c':
				$this->add($name, $data);
			break;
			case 'r':
				return $this->get($name);
			break;
			case 'u':
				$this->update($name, $data);
			break;
			case 'd':
				$this->delete($name);
			break;
		}
	}

	public function add($name, $data = null){
		add_option($name, $data);
	}

	public function update($name, $data = null){
		update_option($name, $data);
	}

	public function delete($name){
		delete_option($name);
	}

	public function get($name){
		return get_option($name);
	}

	public function has_credentials_key(){
		if(
			$this->token('r')
			&& $this->key('r')
			&& $this->broker_id('r')
		){
			return true;
		}
	}

	public function menu_slug(){
		return 'md-api-settings';
	}

	public function url_slug(){
		return 'admin.php?page=' . $this->menu_slug();
	}

	public function init(){
		$this->has_credentials_key();
		$this->config_api_options();
	}

	public function __construct(){
		$this->init();
	}
}
