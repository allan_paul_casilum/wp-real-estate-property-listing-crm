<?php
/**
 *
 * */
class Masterdigm_Settings_ClientLead {
	protected static $instance = null;
	public $md_lead_status = 'md_lead_status';
	public $md_lead_type = 'md_lead_type';
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function lead_status($action = '', $value = ''){
		$prefix = $this->md_lead_status;
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function lead_type($action = '', $value = ''){
		$prefix = $this->md_lead_type;
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function menu_slug(){
		return 'md-client-lead-settings';
	}

	public function url_slug(){
		return 'admin.php?page=' . $this->menu_slug();
	}

	public function __construct(){

	}
}
