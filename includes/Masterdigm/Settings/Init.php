<?php
/**
 *
 * */
class Masterdigm_Settings_Init {
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {
		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}


	public function menu_slug(){
		return 'md-settings';
	}

	public function url_slug(){
		return 'admin.php?page=' . $this->menu_slug();
	}

	public function __construct(){
	}
}
