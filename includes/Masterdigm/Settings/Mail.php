<?php
/**
 *
 * */
class Masterdigm_Settings_Mail {
	protected static $instance = null;
	public $md_settings_mail_subject = 'md_settings_mail_subject';
	public $md_settings_mail_content = 'md_settings_mail_content';
	public $md_settings_mail_server = 'md_settings_mail_server';
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

		/**
	 * update the mail content
	 * */
	public function default_content(){
		$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);
		$content = '<p>%sitename% - New user registration</p>';
		$content .= 'Hello %name%, here are your website credentials<br>';
		$content .= 'Here are your credentials to log and view up-to-date property listings! You can save your favorite properties, email them, share on Facebook and more!<br>';
		$content .= 'Username : %username%<br>';
		$content .= 'Password : %password%<br>';
		$content .= '<p><a href="%loginurl%" title="Login">Please click to login</a></p>';

		return $content;
	}

	public function default_subject(){
		return 'Hello %name%, here are your website credentials';
	}

	public function md_settings_mail_subject($action = '', $value = ''){
		$prefix = $this->md_settings_mail_subject;
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function md_settings_mail_content($action = '', $value = ''){
		$prefix = $this->md_settings_mail_content;
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function get_mail_server(){
		if( !$this->md_settings_mail_server('r') ){
			return get_option('admin_email');
		}
		return $this->md_settings_mail_server('r');
	}

	public function md_settings_mail_server($action = '', $value = ''){
		$prefix = $this->md_settings_mail_server;
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function put_content(){
		$content = $this->md_settings_mail_content('r');
		if( !$content ){
			$content_data = $this->default_content();
			$this->md_settings_mail_content('u', $content_data);
		}
	}

	public function put_subject(){
		$subject = $this->md_settings_mail_subject('r');
		if( !$subject ){
			$subject_data = $this->default_subject();
			$this->md_settings_mail_subject('u', $subject_data);
		}
	}

	public function menu_slug(){
		return 'md-mail-settings';
	}

	public function url_slug(){
		return 'admin.php?page=' . $this->menu_slug();
	}

	public function __construct(){

	}
}
