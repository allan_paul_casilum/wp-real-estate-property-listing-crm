<?php
/**
 *
 * */
class Masterdigm_Settings_PopUp {
	protected static $instance = null;
	public $md_popup_show = 'md_popup_show';
	public $md_popup_close = 'md_popup_close';
	public $md_popup_clicks = 'md_popup_clicks';
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function md_popup_show($action = '', $value = ''){
		$prefix = $this->md_popup_show;
		switch($action){
			case 'u':
				update_option($prefix, $value);
			break;
			case 'd':
				delete_option($prefix, $value);
			break;
			case 'r':
			default:
				return get_option($prefix, $value);
			break;
		}
	}

	public function md_popup_close($action = '', $value = ''){
		$prefix = $this->md_popup_close;
		switch($action){
			case 'u':
				update_option($prefix, $value);
			break;
			case 'd':
				delete_option($prefix, $value);
			break;
			case 'r':
			default:
				return get_option($prefix, $value);
			break;
		}
	}

	public function md_popup_clicks($action = '', $value = ''){
		$prefix = $this->md_popup_clicks;
		switch($action){
			case 'u':
				update_option($prefix, $value);
			break;
			case 'd':
				delete_option($prefix, $value);
			break;
			case 'r':
			default:
				return get_option($prefix, $value);
			break;
		}
	}

	public function show_popup_choose(){
		return array(
			'1' => __('Turn on, show popup after certain clicks?', md_localize_domain()),
			'0' => __('Turn off, do not show popup after certain clicks?', md_localize_domain()),
		);
	}

	public function show_popup_close_button(){
		return array(
			'1' => __('No', md_localize_domain()),
			'0' => __('Yes', md_localize_domain()),
		);
	}

	public function show_popup_after(){
		return array(
			'1' => '1',
			'2' => '2',
			'3' => '3',
			'4' => '4',
			'5' => '5',
		);
	}

	public function menu_slug(){
		return 'md-popup-settings';
	}

	public function url_slug(){
		return 'admin.php?page=' . $this->menu_slug();
	}

	public function __construct(){

	}
}
