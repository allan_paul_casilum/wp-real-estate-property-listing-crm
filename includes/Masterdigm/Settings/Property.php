<?php
/**
 *
 * */
class Masterdigm_Settings_Property {
	protected static $instance = null;
	public $md_enable_masonry = 'md_enable_masonry';
	public $md_search_property = 'md_search_property';
	public $md_property_title = 'md_property_title';
	public $md_bookaviewing_url = 'md_bookaviewing_url';
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function search_property($action = '', $value = ''){
		$prefix = $this->md_search_property;
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	/**
	 * address or tagline
	 * */
	public function property_title($action = '', $value = ''){
		$prefix = $this->md_property_title;
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	/**
	 * book a viewing url
	 * */
	public function bookaviewing_url($action = '', $value = ''){
		$prefix = $this->md_bookaviewing_url;
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function show_default_property_name(){
		return array(
			'address' => __('Address', PLUGIN_NAME),
			'tagline' => __('Tag Line', PLUGIN_NAME),
		);
	}

	public function fields_status(){
		$status = array();
		$fields = md_get_fields();

		if( $fields->fields->status ){
			foreach($fields->fields->status as $key=>$val){
				$status[$key] = $val;
			}
		}
		return $status;
	}

	public function menu_slug(){
		return 'md-property-settings';
	}

	public function url_slug(){
		return 'admin.php?page=' . $this->menu_slug();
	}

	public function __construct(){

	}
}
