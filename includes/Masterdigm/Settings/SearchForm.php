<?php
/**
 *
 * */
class Masterdigm_Settings_SearchForm {
	protected static $instance = null;
	public $md_price_range_ten_start = 'md_price_range_ten_start';
	public $md_price_range_ten_end = 'md_price_range_ten_end';
	public $md_price_range_ten_step = 'md_price_range_ten_step';
	public $md_price_range_hundred_start = 'md_price_range_hundred_start';
	public $md_price_range_hundred_end = 'md_price_range_hundred_end';
	public $md_price_range_hundred_step = 'md_price_range_hundred_step';
	public $md_price_range_million_start = 'md_price_range_million_start';
	public $md_price_range_million_end = 'md_price_range_million_end';
	public $md_price_range_million_step = 'md_price_range_million_step';
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}
	//ten
	public function md_price_range_ten_start($action = '', $value = ''){
		$start = md_price_range_by('ten', 'start');
		$prefix = $this->md_price_range_ten_start;
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix, $start);
			break;
		}
	}

	public function md_price_range_ten_end($action = '', $value = ''){
		$end = md_price_range_by('ten', 'end');
		$prefix = $this->md_price_range_ten_end;
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix, $end);
			break;
		}
	}
	public function md_price_range_ten_step($action = '', $value = ''){
		$step = md_price_range_by('ten', 'step');
		$prefix = $this->md_price_range_ten_step;
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix, $step);
			break;
		}
	}
	//hundred
	public function md_price_range_hundred_start($action = '', $value = ''){
		$start = md_price_range_by('hundred', 'start');
		$prefix = $this->md_price_range_hundred_start;
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix, $start);
			break;
		}
	}
	public function md_price_range_hundred_end($action = '', $value = ''){
		$end = md_price_range_by('hundred', 'end');
		$prefix = $this->md_price_range_hundred_end;
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix, $end);
			break;
		}
	}
	public function md_price_range_hundred_step($action = '', $value = ''){
		$step = md_price_range_by('hundred', 'step');
		$prefix = $this->md_price_range_hundred_step;
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix, $step);
			break;
		}
	}
	//million
	public function md_price_range_million_start($action = '', $value = ''){
		$start = md_price_range_by('million', 'start');
		$prefix = $this->md_price_range_million_start;
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix, $start);
			break;
		}
	}

	public function md_price_range_million_end($action = '', $value = ''){
		$end = md_price_range_by('million', 'end');
		$prefix = $this->md_price_range_million_end;
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix, $end);
			break;
		}
	}

	public function md_price_range_million_step($action = '', $value = ''){
		$step = md_price_range_by('million', 'step');
		$prefix = $this->md_price_range_million_step;
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix, $step);
			break;
		}
	}

	public function menu_slug(){
		return 'md-search-form-settings';
	}

	public function url_slug(){
		return 'admin.php?page=' . $this->menu_slug();
	}

	public function __construct(){

	}
}
