<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
class Masterdigm_Subscribe extends Masterdigm_Init{
	protected static $instance = null;
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function masterdigm_subscribe_transaction_key($action = '', $value = ''){
		$prefix = 'masterdigm_subscribe_transaction_key';
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function masterdigm_subscribe_step_1($action = '', $value = ''){
		$prefix = 'masterdigm_subscribe_step_1';
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function masterdigm_subscribe_email_used($action = '', $value = ''){
		$prefix = 'masterdigm_subscribe_email_used';
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function masterdigm_subscribe_send_activation_code($action = '', $value = ''){
		$prefix = 'masterdigm_subscribe_send_activation_code';
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function masterdigm_subscribe_step_2($action = '', $value = ''){
		$prefix = 'masterdigm_subscribe_step_2';
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function masterdigm_subscribe_msg_step_2($action = '', $value = ''){
		$prefix = 'masterdigm_subscribe_msg_step_2';
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function masterdigm_subscribe_step_2_data($action = '', $value = ''){
		$prefix = 'masterdigm_subscribe_step_2_data';
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function masterdigm_subscribe_step_3($action = '', $value = ''){
		$prefix = 'masterdigm_subscribe_step_3';
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function masterdigm_subscribe_step_3_current_user($action = '', $value = ''){
		$prefix = 'masterdigm_subscribe_step_3_current_user';
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function masterdigm_subscribe_activation_code($action = '', $value = ''){
		$prefix = 'masterdigm_subscribe_activation_code';
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function masterdigm_subscribe_api_step_3($action = '', $value = ''){
		$prefix = 'masterdigm_subscribe_api_step_3';
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function masterdigm_subscribe_api_step_3_msg($action = '', $value = ''){
		$prefix = 'masterdigm_subscribe_api_step_3_msg';
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	public function masterdigm_subscribe_api_finish($action = '', $value = ''){
		$prefix = 'masterdigm_subscribe_api_finish';
		switch($action){
			case 'd':
				delete_option($prefix);
			break;
			case 'u':
				update_option($prefix, $value);
			break;
			case 'r':
				return get_option($prefix);
			break;
		}
	}

	/**
	 * @param $arg array() possible elements
	 * k = the activation key - required
	 * email = email address - required
	 * company = name of the company
	 * first_name = first name
	 * last_name = last name
	 * */
	public function get_transaction_key($arg = array()){
		//http://api.masterdigm.net/fkey?email={email}
		$email = '';
		if( isset($arg['email']) ){
			$email = $arg['email'];
		}

		$url = "http://api.masterdigm.net/fkey?email={$email}";
		$response = wp_remote_get($url);
		$response_code 	= wp_remote_retrieve_response_code( $response );
		$response_body = wp_remote_retrieve_body($response);
		$response_body = json_decode($response_body);
		if( $response_code == 200 ){
			if(
				$response_body
				&& isset($response_body->result)
				&& $response_body->result == 'success'
			){
				$this->masterdigm_subscribe_transaction_key('u', $response_body->key);
				$this->masterdigm_subscribe_step_1('u', 1);
				$this->masterdigm_subscribe_email_used('u', $email);
			}else{
				$this->masterdigm_subscribe_step_1('u', 0);
				$this->masterdigm_subscribe_transaction_key('u', 'fail');
				return false;
			}
		}else{
			$this->masterdigm_subscribe_step_1('u', 0);
			$this->masterdigm_subscribe_transaction_key('u', 'fail');
			return false;
		}
		return false;
	}

		/**
	 * step two
	 * @param $arg array() possible elements
	 * k = the activation key - required
	 * email = email address - required
	 * company = name of the company
	 * first_name = first name
	 * last_name = last name
	 * */
	public function activate_transaction_key($arg = array()){
		//http://api.masterdigm.net/request/activation/code?k={key}&email=test37@mail.com&company={company}&first_name={first_name}&last_name={last_name}
		$key = '';
		if( isset($arg['k']) ){
			$key = $arg['k'];
		}
		$email = '';
		if( isset($arg['email']) ){
			$email = $arg['email'];
		}
		$company = '';
		if( isset($arg['company']) ){
			$company = $arg['company'];
		}
		$first_name = '';
		if( isset($arg['first_name']) ){
			$first_name = $arg['first_name'];
		}
		$last_name = '';
		if( isset($arg['last_name']) ){
			$last_name = $arg['last_name'];
		}

		$url = "http://api.masterdigm.net/request/activation/code?k={$key}&email={$email}&company={$company}&first_name={$first_name}&last_name={$last_name}";
		$response = wp_remote_get($url);
		$response_code = wp_remote_retrieve_response_code( $response );
		$response_body = wp_remote_retrieve_body($response);
		$response_body = json_decode($response_body);
		if( $response_code == 200 ){
			if(
				$response_body
				&& isset($response_body->result)
				&& $response_body->result == 'success'
			){
				$this->masterdigm_subscribe_send_activation_code('u', 1);
				$this->masterdigm_subscribe_step_2('u', 1);
				$this->masterdigm_subscribe_msg_step_2('u', $response_body->message);
				$step_2_data = array(
					'email' => $email,
					'company' => $company,
					'first_name' => $first_name,
					'last_name' => $last_name,
				);
				$this->masterdigm_subscribe_step_2_data('u', $step_2_data);
			}else{
				$this->masterdigm_subscribe_step_2('u', 0);
				$this->masterdigm_subscribe_msg_step_2('u', $response_body->message);
			}
		}else{
			$this->masterdigm_subscribe_step_2('u', 0);
			$this->masterdigm_subscribe_msg_step_2('u', 'fail');
		}
		return false;
	}

	/**
	 * step three
	 * @param $arg array() possible elements
	 * k = the activation code - required
	 * email = email address - required
	 * company = name of the company
	 * first_name = first name
	 * last_name = last name
	 * */
	public function validate_activation_code($arg = array()){
		//http://api.masterdigm.net/activate?email={email}&activation_code={activation_code}
		$api_message 	= false;
		$api_result 	= false;
		$activation_code = '';
		if(
			isset($arg['activation_code'])
			&& trim($arg['activation_code']) != ''
		){
			$activation_code = $arg['activation_code'];
		}
		$email = '';
		if(
			isset($arg['email'])
			&& trim($arg['email']) != ''
		){
			$email = $arg['email'];
		}
		$url = "http://api.masterdigm.net/activate?email={$email}&activation_code={$activation_code}";
		$response 		= wp_remote_get($url);
		$response_code 	= wp_remote_retrieve_response_code( $response );
		$response_body = wp_remote_retrieve_body($response);
		$response_body = json_decode($response_body);
		if( $response_code == 200 ){
			if(
				$response_body
				&& isset($response_body->result)
				&& $response_body->result == 'success'
			){
				$data = array(
					'activation_code' => $activation_code,
					'data_result' => $response_body
				);
				$current_user 	= wp_get_current_user();
				$this->masterdigm_subscribe_step_3('u', 1);
				$this->masterdigm_subscribe_step_3_current_user('u', $current_user);
				$this->masterdigm_subscribe_activation_code('u', $activation_code);
				$this->masterdigm_subscribe_api_step_3('u', $data);
				$api_message = 'success';
				$api_result = true;
				$this->masterdigm_subscribe_api_step_3_msg('u', $api_message);
				return true;
			}else{
				$api_message = $response_body->message;
				$this->masterdigm_subscribe_api_step_3_msg('u', $response_body->message);
				return false;
			}
		}
		return false;
	}
}
