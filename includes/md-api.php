<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$GLOBALS['md_api'] = new Masterdigm_Settings_API;
/**
 * api functions related
 * */
function md_get_api_key(){
	global $md_api;
	return $md_api->key('r');
}
function md_get_api_token(){
	global $md_api;
	return $md_api->token('r');
}
function md_get_api_broker_id(){
	global $md_api;
	return $md_api->broker_id('r');
}
function md_get_api_default_feed(){
	global $md_api;
	return $md_api->default_feed('r');
}
function md_has_api_credentials(){
	global $md_api;
	return $md_api->has_credentials_key();
}
