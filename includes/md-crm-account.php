<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
$GLOBALS['crm'] = new Masterdigm_CRM_API;
function md_get_account_details(){
	global $crm;
	$cache_keyword = 'md_account_details';
	if( cache_get($cache_keyword) ){
		$ret = cache_get($cache_keyword);
	}else{
		cache_set($cache_keyword, $crm->get_account_details());
		$ret = cache_get($cache_keyword);
	}
	return $ret;
}
function md_get_lead_status($output = 'ARRAY'){
	$accnt = md_get_account_details();
	$ret = '';
	$accnt_status = '';
	$cache_keyword = 'md_get_lead_status';
	if(
		$accnt
		&& $accnt->result == 'success'
	){
		switch($output){
			case 'ARRAY':
				$status = array();
				if( $accnt->data->lead_status ){
					foreach($accnt->data->lead_status as $key=>$val){
						$status[$key] = $val;
					}
				}
				$accnt_status = $status;
			break;
			case 'OBJECT':
			default:
				$accnt_status = $accnt->data->lead_status;
			break;
		}
	}
	if( cache_get($cache_keyword) ){
		$ret = cache_get($cache_keyword);
	}else{
		cache_set($cache_keyword, $accnt_status);
		$ret = cache_get($cache_keyword);
	}
	return $ret;
}
function md_get_lead_type($output = 'ARRAY'){
	$accnt = md_get_account_details();
	$ret = '';
	$accnt_type = '';
	$cache_keyword = 'md_get_lead_type';
	if(
		$accnt
		&& $accnt->result == 'success'
	){
		switch($output){
			case 'ARRAY':
				$lead_type = array();
				if( $accnt->data->lead_type ){
					foreach($accnt->data->lead_type as $key=>$val){
						$lead_type[$key] = $val;
					}
				}
				$accnt_type = $lead_type;
			break;
			case 'OBJECT':
			default:
				$accnt_type = $accnt->data->lead_type;
			break;
		}
	}
	if( cache_get($cache_keyword) ){
		$ret = cache_get($cache_keyword);
	}else{
		cache_set($cache_keyword, $accnt_type);
		$ret = cache_get($cache_keyword);
	}
	return $ret;
}
function md_get_fields(){
	global $crm;
	$cache_keyword = 'md_account_fields';
	$ret = '';
	if( cache_get($cache_keyword) ){
		$ret = cache_get($cache_keyword);
	}else{
		cache_set($cache_keyword, $crm->get_fields());
		$ret = cache_get($cache_keyword);
	}
	return $ret;
}
