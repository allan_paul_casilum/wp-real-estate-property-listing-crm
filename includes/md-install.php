<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 * install / setup functions related
 * */
$install = new Masterdigm_InstallNotice;
function md_is_first_time_activated(){
	global $md_config;
	$option_prefix = $md_config['api']['active_option_prefix'];
	return get_option($option_prefix);
}
function md_has_passed_prerequisite($action = 'r'){
	global $install;
	return $install->has_passed_prerequisite($action);
}
function md_finish_subscribe(){
	$subscribe = new Masterdigm_Subscribe;
	return $subscribe->masterdigm_subscribe_api_finish('r');
}
