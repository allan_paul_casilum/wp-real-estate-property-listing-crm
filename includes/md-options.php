<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
/**
 * Functions for table options
 *
 * options name for globally use
 * */
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

function cleanup_options(){
	/*md_api_key('d');
	md_api_token('d');
	md_options_plugin_settings('d');
	md_options_finish_install('d');*/
	//create function to query _options, *masterdigm_* | *md_* and delete them
	md_cleanup_db('masterdigm_');
	md_cleanup_db('md_');
	md_cleanup_db('save-search');
	md_cleanup_db('log_crm');
	md_cleanup_db('create_page_by');
	md_cleanup_db('property_data_feed');
	md_cleanup_db('user_id');
	md_cleanup_db('broker_id');
	md_cleanup_db('mls_');
	md_cleanup_db('social-api');
	md_cleanup_db('success-unsubscribe');
	md_cleanup_db('fail-unsubscribe');
}
function md_get_options_db($option_name){
	global $wpdb;
	$sql = "SELECT * FROM $wpdb->options WHERE option_name like '%".$option_name."%'";
	return $wpdb->get_results($sql);
}
function md_cleanup_db($option_name){
	$get = md_get_options_db($option_name);
	if( count($get) > 0 ){
		foreach($get as $key => $val){
			delete_option($val->option_name);
		}
	}
}
/**
 * get options
 *
 * get the options save
 *
 * @var $key	mix		name of the option
 * @var	$edfault	mixed | optional The default value to return if no value is returned (ie. the option is not in the database).
 * @see https://codex.wordpress.org/Function_Reference/get_option
 * @return get_option function
 * */
function md_get_options($key, $default = ''){
	return get_option($key, $default);
}
/**
 * update options
 *
 * @var $key	(string) (required) Name of the option to update. Must not exceed 64 characters. A list of valid default options to update can be
 * @var $value	(mixed) (required) The NEW value for this option name. This value can be an integer, string, array, or object.
 * */
function md_update_options($key, $value){
	update_option($key, $value);
}
