<?php
/**
 * Masterdigm CRM API - Property and Account
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * Dashboard. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 *
 * @link              http://masterdigm.com
 * @since             1.0.0
 * @package           Masterdigm_Api
 *
 * @wordpress-plugin
 * Plugin Name:       Masterdigm Real Estate
 * Plugin URI:        http://www.masterdigm.com/realestatewordpressplugin
 * Description:		  Used by Professional Real Estate companies around the world! A true all-in-one WP real estate solution Visit plugin page : <a href="http://www.masterdigm.com/masterdigm-plugin-documentation" target="_blank">http://www.masterdigm.com/masterdigm-plugin-documentation</a>
 * Version:           3.33.25
 * Author:            Masterdigm
 * Author URI:        http://masterdigm.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       masterdigm-api
 * Domain Path:       /languages
 * Bitbucket Plugin URI: https://bitbucket.org/allan_paul_casilum/wp-real-estate-property-listing-crm
 * Bitbucket Branch:     master
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}
define('Masterdigm', 1);
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-plugin-name-activator.php
 */
function activate_plugin_name() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-real-estate-property-listing-crm-activator.php';
	WP_Real_Estate_Property_Listing_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-plugin-name-deactivator.php
 */
function deactivate_plugin_name() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-wp-real-estate-property-listing-crm-deactivator.php';
	WP_Real_Estate_Property_Listing_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_plugin_name' );
register_deactivation_hook( __FILE__, 'deactivate_plugin_name' );

//config
require_once plugin_dir_path( __FILE__ ) . 'config.php';
//autoload the include/class
spl_autoload_register( 'masterdigm_load_include_class' );
function masterdigm_load_include_class( $class_name ) {
	if ( false !== strpos( $class_name, 'Masterdigm' ) ) {
		$include_classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'includes' . DIRECTORY_SEPARATOR;
		$admin_classes_dir = realpath( plugin_dir_path( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'admin' . DIRECTORY_SEPARATOR;
		$class_file = str_replace( '_', DIRECTORY_SEPARATOR, $class_name ) . '.php';
		if( file_exists($include_classes_dir . $class_file) ){
			require_once $include_classes_dir . $class_file;
		}
		if( file_exists($admin_classes_dir . $class_file) ){
			require_once $admin_classes_dir . $class_file;
		}
	}
}
//load library
require_once plugin_dir_path( __FILE__ ) . 'includes/phpfastcache/3.0.0/phpfastcache.php';
//load include functions
require_once plugin_dir_path( __FILE__ ) . 'includes/md-cache.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/md-options.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/md-install.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/md-api.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/md-crm.php';
require_once plugin_dir_path( __FILE__ ) . 'includes/md-crm-account.php';

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-wp-real-estate-property-listing-crm.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_plugin() {
	global $wp_scripts;
	$plugin = new WP_Real_Estate_Property_Listing();
	$plugin->run();
	add_action( 'plugins_loaded', 'init_cache' );
	md_get_account_details();
	//auto add data
	Masterdigm_Controller_Settings_Mail::get_instance()->controller('auto_add_data');
}
run_plugin();

if( is_admin() ){
	//load admin dependencies
	require plugin_dir_path( __FILE__ ) . 'admin/admin.php';
}
